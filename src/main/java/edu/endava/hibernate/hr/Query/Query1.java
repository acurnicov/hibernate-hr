package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query1 {
    private String employeeFirstName;
    private String employeeLastName;
    private Integer departmentId;
    private String departmentName;
}
