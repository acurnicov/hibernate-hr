package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query7 {
    private String employeeFirstName;
    private String employeeLastName;
    private float employeeSalary;
}
