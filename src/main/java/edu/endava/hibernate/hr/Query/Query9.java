package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query9 {
    private String departmentName;
    private String city;
    private String stateProvince;
}