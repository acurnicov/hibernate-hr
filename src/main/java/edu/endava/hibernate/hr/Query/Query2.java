package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query2 {
    private String employeeFirstName;
    private String employeeLastName;
    private String departmentName;
    private String city;
    private String stateProvince;
}
