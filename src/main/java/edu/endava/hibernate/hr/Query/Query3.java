package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query3 {
    private String employeeFirstName;
    private String employeeLastName;
    private float employeeSalary;
    private String jobTitle;
}
