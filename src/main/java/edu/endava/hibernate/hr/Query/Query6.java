package edu.endava.hibernate.hr.Query;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Query6 {
    private String departmentName;
}
