package edu.endava.hibernate.hr.Entity;

import edu.endava.hibernate.hr.Entity.Country;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "regions")
public class Region {
    @Id
    @Column(name = "region_id")
    private Integer id;

    @Column(name = "region_name")
    private String name;

    @OneToMany(mappedBy = "region")
    private List<Country> countries = new ArrayList<>();

    @Override
    public String toString() {
        return "[" + id + ": " + name + "]";
    }
}