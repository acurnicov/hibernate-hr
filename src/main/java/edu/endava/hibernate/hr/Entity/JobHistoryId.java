package edu.endava.hibernate.hr.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Date;

@Data
@Embeddable
@AllArgsConstructor
public class JobHistoryId implements Serializable {
    public JobHistoryId() {
    }

    @Column(name = "employee_id")
    private Integer id;

    @Column(name = "start_date")
    private Date startDate;
}