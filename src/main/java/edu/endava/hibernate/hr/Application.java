package edu.endava.hibernate.hr;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("edu.endava.hibernate.hr");
        EntityManager em = emf.createEntityManager();

        List query1 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query1(e.firstName, e.lastName, d.id, d.name) " +
                "from Employee e inner join e.department d").getResultList();

        List query2 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query2(e.firstName, e.lastName, d.name, l.city, l.stateProvince) " +
                "from Employee e inner join e.department d inner join d.location l").getResultList();

        List query3 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query3(e.firstName, e.lastName, e.salary, j.title) " +
                "from Employee e inner join e.job j").getResultList();

        List query4 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query1(e.firstName, e.lastName, d.id, d.name) " +
                "from Employee e inner join e.department d WHERE d.id = 40 OR d.id = 80").getResultList();

        List query5 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query2(e.firstName, e.lastName, d.name, l.city, l.stateProvince) " +
                "from Employee e inner join e.department d inner join d.location l WHERE lower(e.firstName) LIKE '%z%'").getResultList();

        List query6 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query6(d.name) " +
                "from Department d").getResultList();

        List query7 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query7(e.firstName, e.lastName, e.salary) " +
                "from Employee e inner join e.job j WHERE e.salary < (" +
                "select salary from Employee e2 where e2.id = 182)").getResultList();

        List query8 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query8(e.firstName, m.firstName) " +
                "from Employee e inner join e.manager m").getResultList();

        List query9 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query9(d.name, l.city, l.stateProvince) " +
                "from Department d inner join d.location l").getResultList();

        List query10 = em.createQuery("select new edu.endava.hibernate.hr.Query.Query1(e.firstName, e.lastName, d.id, d.name) " +
                "from Employee e left join e.department d").getResultList();

        System.out.println("Query 1:" + query1);
        System.out.println("Query 2:" + query2);
        System.out.println("Query 3:" + query3);
        System.out.println("Query 4:" + query4);
        System.out.println("Query 5:" + query5);
        System.out.println("Query 6:" + query6);
        System.out.println("Query 7:" + query7);
        System.out.println("Query 8:" + query8);
        System.out.println("Query 9:" + query9);
        System.out.println("Query 10:" + query10);
    }
}
